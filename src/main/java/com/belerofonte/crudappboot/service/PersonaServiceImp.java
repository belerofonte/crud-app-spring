package com.belerofonte.crudappboot.service;

import com.belerofonte.crudappboot.commons.GenericServiceImp;
import com.belerofonte.crudappboot.dao.IPersonaDAO;
import com.belerofonte.crudappboot.models.Persona;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class PersonaServiceImp extends GenericServiceImp<Persona, Long> implements IPersonaService {

    @Autowired
	private IPersonaDAO personaDao;
	

	@Override
	public CrudRepository<Persona, Long> getDao() {
		return this.personaDao;
	}


	@Override
	public Page<Persona> getAll(Pageable pageable) {	
        return personaDao.findAll(pageable);
	}
    
}