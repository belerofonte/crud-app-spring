package com.belerofonte.crudappboot.service;

import com.belerofonte.crudappboot.commons.IGenericService;
import com.belerofonte.crudappboot.models.Persona;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IPersonaService extends IGenericService<Persona, Long>{
    Page<Persona> getAll(Pageable pageable);
}