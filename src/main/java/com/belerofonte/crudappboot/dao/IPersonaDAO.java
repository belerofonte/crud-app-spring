package com.belerofonte.crudappboot.dao;

import com.belerofonte.crudappboot.models.Persona;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaDAO extends JpaRepository<Persona, Long> {
    
    
}