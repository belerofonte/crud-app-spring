package com.belerofonte.crudappboot.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Table(name = "persona")
public class Persona {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    @Id
	@Getter @Setter private Long id;
    @Column
	@Getter @Setter private String nombre;	
    @Column
	@Getter @Setter private String apellido;	
    @Column
	@Getter @Setter private String direccion;	
    @Column
    @Getter @Setter private String telefono;
}