package com.belerofonte.crudappboot.controllers;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.belerofonte.crudappboot.models.Persona;
import com.belerofonte.crudappboot.service.IPersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class IndexController {
    @Autowired
    private IPersonaService personaService;

    @GetMapping({"/", "/index", " "})  
    public String index(@RequestParam Map<String, Object> params, Model model) {
        int page = params.get("page") != null ? (Integer.valueOf(params.get("page").toString()) - 1) : 0;
        
        PageRequest pageRequest = PageRequest.of(page, 5);
		
		Page<Persona> pagePersona = personaService.getAll(pageRequest);
		
		int totalPage = pagePersona.getTotalPages();
		if(totalPage > 0) {
			List<Integer> pages = IntStream.rangeClosed(1, totalPage).boxed().collect(Collectors.toList());
			model.addAttribute("pages", pages);
		}
		
		model.addAttribute("list", pagePersona.getContent());
		model.addAttribute("current", page + 1);
		model.addAttribute("next", page + 2);
		model.addAttribute("prev", page);
		model.addAttribute("last", totalPage);
        
        return "home";
    }
    
    @GetMapping("/save/{id}") 
    public String showSave(@PathVariable("id") Long id, Model model){
        if (id != null && id != 0 ) {
            model.addAttribute("persona", personaService.get(id));
        } else {
            model.addAttribute("persona", new Persona());
        }
        return "save";
    }

    @PostMapping(value="/save")
    public String save(Persona persona, Model model) {  
        personaService.save(persona);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id, Model model) {
        personaService.delete(id);
        return "redirect:/";
    }

}